/**
 * Created by aldri on 17/11/2015.
 */
const gulp            = require('gulp');
const mainBowerFiles  = require('gulp-main-bower-files');
const rename          = require('gulp-rename');
const concat          = require('gulp-concat');
const clean           = require('gulp-clean');
const _               = require('underscore');
/*
    Javascript
 */
const coffee      = require('gulp-coffee');
const ngAnnotate  = require('gulp-ng-annotate');
const jsmin       = require('gulp-jsmin');
/*
    CSS
 */
const prefixer    = require('gulp-autoprefixer');
const minifyCSS   = require('gulp-minify-css');

const sass        = require('gulp-ruby-sass');
/*
    Essential
 */
const plumber     = require('gulp-plumber');

//Angular Files
gulp.task('ang-ctrl',function(){
  gulp.src('app/**/*.coffee')
      .pipe(plumber())
      .pipe(coffee({bare: true}))
      .pipe(ngAnnotate({single_quotes:true}))
      .pipe(concat('angular-dep.min.js'))
      .pipe(jsmin())
      .pipe(gulp.dest('../src/scripts'));
});

//Styles
/*
  Sass Components
*/
var processSass = function(config){
  return sass(config.src)
             .pipe(plumber())
             .on('error',sass.logError)
             .pipe(prefixer());
}

gulp.task("sass-template",function(){
  var config = {
    src :"styles/**/*.sass",
    dest:"../src/styles"
  };
  var task = processSass(config);
  task.pipe(minifyCSS())
      .pipe(gulp.dest(config.dest));
});

/*
    Run-watch
 */
gulp.task('watch',function(){
    gulp.watch('app/**/*.coffee',['ang-ctrl']);
    gulp.watch('styles/**/*.sass',['sass-template']);
});
gulp.task('default',['ang-ctrl','sass-template','watch']);

gulp.task('load-dep',function(){
  /*
    Include the UI Kit components
  */
  var overrides = {
    uikit:{
      main:[
        './css/uikit.css',
        './js/uikit.js'
        ]
      }
  }

  gulp.src('./bower.json')
      .pipe(mainBowerFiles({overrides:overrides}))
      .pipe(gulp.dest('vendors'));

  gulp.src('bower_components/**/*.{ttf,otf,woff,woff2}')
      .pipe(rename({dirname:''}))
      .pipe(gulp.dest('../src /fonts'));

  /*
    Write the Dependencies and concat them
  */
  var vendorDest    = 'vendors/';
  var JSdependencies = {
      foundations: [
        vendorDest + 'jquery/dist/jquery.js'
      ],
      uiKit : [
        vendorDest + 'uikit/js/uikit.js'
      ],
      angular : [
        vendorDest + 'angular/angular.js'
      ]
    }

  var JSdependenciesFlat = _.flatten([
    JSdependencies.foundations,
    JSdependencies.uiKit,
    JSdependencies.angular
  ]);

  gulp.src(JSdependenciesFlat)
      .pipe(plumber())
      .pipe(ngAnnotate({single_quotes:true}))
      .pipe(concat('js-dep.min.js'))
      .pipe(jsmin())
      .pipe(gulp.dest('../src/scripts'));

  var cssDependencies = [
    vendorDest + 'uikit/css/uikit.css'
  ];
  gulp.src(cssDependencies)
      .pipe(plumber())
      .pipe(concat('css-dep.min.css'))
      .pipe(minifyCSS())
      .pipe(gulp.dest('../src/styles'));
});
