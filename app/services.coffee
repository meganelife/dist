als.factory 'aniList', ($http) ->
  auth: (cb) ->
    args = ['clientID','clientSecret']
    chrome.storage.sync.get args, (items) ->
      url = "https://anilist.co/api"
      req =
        grant_type   : 'client_credentials',
        client_id    : items.clientID,
        client_secret: items.clientSecret
      urlReq = url+'/auth/access_token'
      q = $http.post urlReq,req
      q.success (res) ->
        cb(res)
      q.error (res) ->
        cb(res)

  search: (params,cb) ->
    this.auth (res) ->
      url = "https://anilist.co/api"
      urlReq = url+"/"+params.type+"/search/"+params.series
      req =
        method:'GET'
        url: urlReq
        params:
          access_token: res.access_token

      q = $http req
      q.success (res) ->
        cb(res)
      q.error (res) ->
        cb(res)
