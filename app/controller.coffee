als.controller "main", ($scope) ->
  $scope.menus =
    settings: false
    search: true
    result: false

  $scope.types = ['anime','manga']

  $scope.search = ->
    $scope.results = [1,2,3,4,5,6,7,8,9,10]
    $scope.changeMenu "result"

  $scope.changeMenu = (key) ->
    angular.forEach $scope.menus, (val,key) ->
      $scope.menus[key] = false
    $scope.menus[key] = true
    console.log $scope.menus
